﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Admin/Login/
        public ActionResult Index()
        {
            return View();
        }

        // Kullanıcı adı şifre kontrol ediliyor 
        // kriterler doğru ise yönetim paneline giriş yapılıyor
        // değilse giriş sayfasına yönlendiriliyor
        [HttpPost]
        public ActionResult DoLogin(string username, string password, string remember)
        {
            if (remember == "1")
            {
                return RedirectToAction("Index", "Dashboard");
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
    }
}