using System;
using System.Collections.Generic;

namespace Blog.Models
{
    public partial class Yorum
    {
        public int Id { get; set; }
        public string Baslik { get; set; }
        public string Icerik { get; set; }
        public int MakaleID { get; set; }
        public System.DateTime Tarih { get; set; }
        public virtual Makale Makale { get; set; }
    }
}
