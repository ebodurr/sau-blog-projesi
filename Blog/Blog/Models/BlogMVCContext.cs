using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Blog.Models.Mapping;

namespace Blog.Models
{
    public partial class BlogMVCContext : DbContext
    {
        static BlogMVCContext()
        {
            Database.SetInitializer<BlogMVCContext>(null);
        }

        public BlogMVCContext()
            : base("Name=BlogMVCContext")
        {
        }

        public DbSet<Ayarlar> Ayarlars { get; set; }
        public DbSet<Dil> Dils { get; set; }
        public DbSet<Etiket> Etikets { get; set; }
        public DbSet<Kategori> Kategoris { get; set; }
        public DbSet<Makale> Makales { get; set; }
        public DbSet<Ortam> Ortams { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<Yazar> Yazars { get; set; }
        public DbSet<Yorum> Yorums { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AyarlarMap());
            modelBuilder.Configurations.Add(new DilMap());
            modelBuilder.Configurations.Add(new EtiketMap());
            modelBuilder.Configurations.Add(new KategoriMap());
            modelBuilder.Configurations.Add(new MakaleMap());
            modelBuilder.Configurations.Add(new OrtamMap());
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new YazarMap());
            modelBuilder.Configurations.Add(new YorumMap());
        }
    }
}
