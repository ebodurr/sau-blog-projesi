using System;
using System.Collections.Generic;

namespace Blog.Models
{
    public partial class Yazar
    {
        public Yazar()
        {
            this.Makales = new List<Makale>();
            this.Ortams = new List<Ortam>();
        }

        public System.Guid Id { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }
        public string Eposta { get; set; }
        public System.DateTime KayitTarihi { get; set; }
        public string KullaniciAdi { get; set; }
        public Nullable<int> ResimID { get; set; }
        public Nullable<int> DilID { get; set; }
        public bool Aktif { get; set; }
        public virtual Dil Dil { get; set; }
        public virtual ICollection<Makale> Makales { get; set; }
        public virtual ICollection<Ortam> Ortams { get; set; }
        public virtual Ortam Ortam { get; set; }
    }
}
