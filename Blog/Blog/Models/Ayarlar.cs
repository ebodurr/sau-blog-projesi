using System;
using System.Collections.Generic;

namespace Blog.Models
{
    public partial class Ayarlar
    {
        public int Id { get; set; }
        public string SiteAdi { get; set; }
        public string SiteKokAdresi { get; set; }
        public string SiteAciklama { get; set; }
        public string AnahtarKelimeler { get; set; }
        public bool SiteYayinda { get; set; }
        public string SiteLogo { get; set; }
        public int SiteDili { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string GooglePlus { get; set; }
        public string Eposta { get; set; }
        public virtual Dil Dil { get; set; }
    }
}
