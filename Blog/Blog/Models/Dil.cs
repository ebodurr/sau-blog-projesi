using System;
using System.Collections.Generic;

namespace Blog.Models
{
    public partial class Dil
    {
        public Dil()
        {
            this.Ayarlars = new List<Ayarlar>();
            this.Makales = new List<Makale>();
            this.Yazars = new List<Yazar>();
        }

        public int Id { get; set; }
        public string Adi { get; set; }
        public string Kod { get; set; }
        public virtual ICollection<Ayarlar> Ayarlars { get; set; }
        public virtual ICollection<Makale> Makales { get; set; }
        public virtual ICollection<Yazar> Yazars { get; set; }
    }
}
