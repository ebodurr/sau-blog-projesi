using System;
using System.Collections.Generic;

namespace Blog.Models
{
    public partial class Ortam
    {
        public Ortam()
        {
            this.Makales = new List<Makale>();
            this.Yazars = new List<Yazar>();
            this.Makales1 = new List<Makale>();
        }

        public int Id { get; set; }
        public string Adi { get; set; }
        public string MinPath { get; set; }
        public string MedPath { get; set; }
        public string LargePath { get; set; }
        public System.DateTime EklenmeTarihi { get; set; }
        public int Goruntuleme { get; set; }
        public Nullable<System.Guid> EkleyenYazarID { get; set; }
        public int Begeni { get; set; }
        public virtual ICollection<Makale> Makales { get; set; }
        public virtual Yazar Yazar { get; set; }
        public virtual ICollection<Yazar> Yazars { get; set; }
        public virtual ICollection<Makale> Makales1 { get; set; }
    }
}
