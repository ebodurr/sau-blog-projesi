using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Blog.Models.Mapping
{
    public class AyarlarMap : EntityTypeConfiguration<Ayarlar>
    {
        public AyarlarMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.SiteAdi)
                .HasMaxLength(150);

            this.Property(t => t.Facebook)
                .HasMaxLength(50);

            this.Property(t => t.Twitter)
                .HasMaxLength(50);

            this.Property(t => t.GooglePlus)
                .HasMaxLength(150);

            this.Property(t => t.Eposta)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("Ayarlar");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SiteAdi).HasColumnName("SiteAdi");
            this.Property(t => t.SiteKokAdresi).HasColumnName("SiteKokAdresi");
            this.Property(t => t.SiteAciklama).HasColumnName("SiteAciklama");
            this.Property(t => t.AnahtarKelimeler).HasColumnName("AnahtarKelimeler");
            this.Property(t => t.SiteYayinda).HasColumnName("SiteYayinda");
            this.Property(t => t.SiteLogo).HasColumnName("SiteLogo");
            this.Property(t => t.SiteDili).HasColumnName("SiteDili");
            this.Property(t => t.Facebook).HasColumnName("Facebook");
            this.Property(t => t.Twitter).HasColumnName("Twitter");
            this.Property(t => t.GooglePlus).HasColumnName("GooglePlus");
            this.Property(t => t.Eposta).HasColumnName("Eposta");

            // Relationships
            this.HasRequired(t => t.Dil)
                .WithMany(t => t.Ayarlars)
                .HasForeignKey(d => d.SiteDili);

        }
    }
}
