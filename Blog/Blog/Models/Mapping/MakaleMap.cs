using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Blog.Models.Mapping
{
    public class MakaleMap : EntityTypeConfiguration<Makale>
    {
        public MakaleMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Baslik)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Icerik)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("Makale");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Baslik).HasColumnName("Baslik");
            this.Property(t => t.Icerik).HasColumnName("Icerik");
            this.Property(t => t.YayimTarihi).HasColumnName("YayimTarihi");
            this.Property(t => t.KategoriID).HasColumnName("KategoriID");
            this.Property(t => t.YazarID).HasColumnName("YazarID");
            this.Property(t => t.KapakResimID).HasColumnName("KapakResimID");
            this.Property(t => t.DilID).HasColumnName("DilID");
            this.Property(t => t.Aktif).HasColumnName("Aktif");
            this.Property(t => t.OkunmaSayisi).HasColumnName("OkunmaSayisi");

            // Relationships
            this.HasMany(t => t.Ortams)
                .WithMany(t => t.Makales1)
                .Map(m =>
                    {
                        m.ToTable("MakaleOrtam");
                        m.MapLeftKey("MakaleId");
                        m.MapRightKey("OrtamId");
                    });

            this.HasOptional(t => t.Dil)
                .WithMany(t => t.Makales)
                .HasForeignKey(d => d.DilID);
            this.HasRequired(t => t.Kategori)
                .WithMany(t => t.Makales)
                .HasForeignKey(d => d.KategoriID);
            this.HasOptional(t => t.Ortam)
                .WithMany(t => t.Makales)
                .HasForeignKey(d => d.KapakResimID);
            this.HasRequired(t => t.Yazar)
                .WithMany(t => t.Makales)
                .HasForeignKey(d => d.YazarID);

        }
    }
}
