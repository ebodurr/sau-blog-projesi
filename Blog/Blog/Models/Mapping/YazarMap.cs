using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Blog.Models.Mapping
{
    public class YazarMap : EntityTypeConfiguration<Yazar>
    {
        public YazarMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Adi)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Soyadi)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Eposta)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.KullaniciAdi)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Yazar");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Adi).HasColumnName("Adi");
            this.Property(t => t.Soyadi).HasColumnName("Soyadi");
            this.Property(t => t.Eposta).HasColumnName("Eposta");
            this.Property(t => t.KayitTarihi).HasColumnName("KayitTarihi");
            this.Property(t => t.KullaniciAdi).HasColumnName("KullaniciAdi");
            this.Property(t => t.ResimID).HasColumnName("ResimID");
            this.Property(t => t.DilID).HasColumnName("DilID");
            this.Property(t => t.Aktif).HasColumnName("Aktif");

            // Relationships
            this.HasOptional(t => t.Dil)
                .WithMany(t => t.Yazars)
                .HasForeignKey(d => d.DilID);
            this.HasOptional(t => t.Ortam)
                .WithMany(t => t.Yazars)
                .HasForeignKey(d => d.ResimID);

        }
    }
}
