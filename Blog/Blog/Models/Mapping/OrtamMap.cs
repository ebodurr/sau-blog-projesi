using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Blog.Models.Mapping
{
    public class OrtamMap : EntityTypeConfiguration<Ortam>
    {
        public OrtamMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Adi)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Ortam");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Adi).HasColumnName("Adi");
            this.Property(t => t.MinPath).HasColumnName("MinPath");
            this.Property(t => t.MedPath).HasColumnName("MedPath");
            this.Property(t => t.LargePath).HasColumnName("LargePath");
            this.Property(t => t.EklenmeTarihi).HasColumnName("EklenmeTarihi");
            this.Property(t => t.Goruntuleme).HasColumnName("Goruntuleme");
            this.Property(t => t.EkleyenYazarID).HasColumnName("EkleyenYazarID");
            this.Property(t => t.Begeni).HasColumnName("Begeni");

            // Relationships
            this.HasOptional(t => t.Yazar)
                .WithMany(t => t.Ortams)
                .HasForeignKey(d => d.EkleyenYazarID);

        }
    }
}
