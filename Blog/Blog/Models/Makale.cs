using System;
using System.Collections.Generic;

namespace Blog.Models
{
    public partial class Makale
    {
        public Makale()
        {
            this.Yorums = new List<Yorum>();
            this.Etikets = new List<Etiket>();
            this.Ortams = new List<Ortam>();
        }

        public int Id { get; set; }
        public string Baslik { get; set; }
        public string Icerik { get; set; }
        public System.DateTime YayimTarihi { get; set; }
        public int KategoriID { get; set; }
        public System.Guid YazarID { get; set; }
        public Nullable<int> KapakResimID { get; set; }
        public Nullable<int> DilID { get; set; }
        public bool Aktif { get; set; }
        public int OkunmaSayisi { get; set; }
        public virtual Dil Dil { get; set; }
        public virtual Kategori Kategori { get; set; }
        public virtual Ortam Ortam { get; set; }
        public virtual Yazar Yazar { get; set; }
        public virtual ICollection<Yorum> Yorums { get; set; }
        public virtual ICollection<Etiket> Etikets { get; set; }
        public virtual ICollection<Ortam> Ortams { get; set; }
    }
}
