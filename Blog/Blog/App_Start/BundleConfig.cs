﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Blog
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254726
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/content/js").Include(
                  "~/Content/assets/plugins/jquery-1.10.2.min.js",
                  "~/Content/assets/plugins/jquery-migrate-1.2.1.min.js",
                  "~/Content/assets/plugins/bootstrap/js/bootstrap.min.js",
                  "~/Content/assets/plugins/back-to-top.js",
                  "~/Content/assets/scripts/app.js"
                  ));

            bundles.Add(new StyleBundle("~/content/css").Include(
                        "~/Content/assets/css/google.css?family=Open+Sans:400,300,600,700&subset=latin,latin-ext",
                        "~/Content/assets/plugins/font-awesome/css/font-awesome.min.css",
                        "~/Content/assets/plugins/bootstrap/css/bootstrap.min.css",
                        "~/Content/assets/css/style-metronic.css",
                        "~/Content/assets/fonts/font.css",
                        "~/Content/assets/css/style.css",
                        "~/Content/assets/css/themes/blue.css",
                        "~/Content/assets/css/style-responsive.css",
                        "~/Content/assets/css/custom.css"
                       ));

            BundleTable.EnableOptimizations = true;
        }
    }
}